# Push and pop directories on directory stack
alias pu='pushd'
alias po='popd'

# Basic directory operations
alias ...='cd ../..'
alias -- -='cd -'

# Super user
alias _='sudo'
alias please='sudo'

#alias g='grep -in'

# Show history
if [ "$HIST_STAMPS" = "mm/dd/yyyy" ]
then
    alias history='fc -fl 1'
elif [ "$HIST_STAMPS" = "dd.mm.yyyy" ]
then
    alias history='fc -El 1'
elif [ "$HIST_STAMPS" = "yyyy-mm-dd" ]
then
    alias history='fc -il 1'
else
    alias history='fc -l 1'
fi
# List direcory contents

# alias emacs='emacs -r'
# alias e='emacs -r'

# ruby
alias rfind='find . -name "*.rb" | xargs grep -n'


alias rake='bundle exec rake'
# alias rspec="RAILS_ENV='test' bundle exec spec"

alias gpgsym-enc="gpg --symmetric -a"
alias gpgsym-dec="gpg -d"
alias osmtest="RAILS_ENV='test' bundle exec rake spec"
alias ansideploy="cd ~/workspace/arkena/ansible-playbooks && sudo ./MakeLocal && cd -"
alias osmstg="rake tf1:staging heroku:deploy"
alias osmprod="rake tf1:production heroku:deploy"

alias gfeat="git flow feature"
alias grel="git flow release"
alias rspec="RAILS_ENV='test' bundle exec rspec"
alias etcd-server="sudo docker run -d -v /containers/etcd:/data.etcd --net=host quay.io/coreos/etcd:v3.0.9 \
    /usr/local/bin/etcd \
    --data-dir=data.etcd --name etcd\
    --initial-advertise-peer-urls http://127.0.0.1:2380 --listen-peer-urls http://127.0.0.1:2380 \
    --advertise-client-urls http://127.0.0.1:2379 --listen-client-urls http://127.0.0.1:2379 \
    --initial-cluster etcd=http://127.0.0.1:2380 --initial-cluster-state new --initial-cluster-token etcdtoken"
alias ucode='sudo apt-get update && sudo apt-get install code-insiders -y'
alias code="code-insiders -g"
alias kp="kubectl --namespace kpm"
alias kn="kubectl --namespace nginx"
alias kt="kubectl --namespace tradetrack"
alias k="kubectl"
alias ks="kubectl --namespace=kube-system"
alias kl="kubectl --namespace=elk"
    