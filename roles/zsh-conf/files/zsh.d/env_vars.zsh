export DEBEMAIL=2t.antoine@gmail.com
export DEBFULLNAME=Antoine Legrand
export PATH="$HOME/.rbenv/bin:$PATH"

export FANCY_FORMAT=name,state,ipv4,ipv6,memory,ram,swap,autostart
export LXCPATH=/containers
